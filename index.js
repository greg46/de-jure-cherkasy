$(function () {
    

    $(window).scroll(function () {

        if ($(this).scrollTop() > 0) {

            $('#navigation').addClass("fixNavigation");
        } else {

            $('#navigation').removeClass("fixNavigation");
        }
    });
});

$(function () {
    $("#btnTop").click(function () {
        $("html, body").animate({ scrollTop: 0 }, "slow");
    });
});

$(function () {
    $("#ПРО_НАС").click(function () {
        $("html, body").animate({ scrollTop: 0 }, "slow");
    });
});



$(function(){
    $("#НАШІ_СТАТТІ").click(function(){
        $("html, body").animate({scrollTop: $("#titreArticle").offset().top},"slow");
    });
});

$(function(){
    $("#ПОСЛУГИ").click(function(){
        $("html, body").animate({scrollTop: $("#pratics").offset().top},"slow");
    });
});

$(function(){
    $("#КОНТАКТИ").click(function(){
        $("html, body").animate({scrollTop: $("#contact").offset().top},"slow");
    });
});

$(function(){
    $("#ЗВОРОТНІЙ_ЗВ’ЯЗОК").click(function(){
        $("html, body").animate({scrollTop: $("#comments").offset().top},"slow");
    });
});

$(function(){
    $("#НАШІ_СТАТТІ").click(function(){
        $("html, body").animate({scrollTop: $("#titreArticle").offset().top},"slow");
    });
});


const sr = ScrollReveal();

sr.reveal('.titreNous', {
    origin: 'top',
    duration: 1500,
    reset: 'true'
});

sr.reveal('.nosArticles', {
    origin: 'top',
    duration: 2000,
    scale: 0.3
});

// sr.reveal('#cards', {
//     origin: 'top',
//     duration: 2000,
//     scale: 0.4
// });

sr.reveal('.contact', {
    origin: 'top',
    duration: 2000,
    scale: 0.3
})